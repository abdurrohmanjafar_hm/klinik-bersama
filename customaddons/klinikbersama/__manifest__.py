# -*- coding: utf-8 -*-
{
    'name': "klinikbersama",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "My Company",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/15.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'report_xlsx'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
        'views/menu.xml',
        'views/person_menu.xml',
        'views/pasien_menu.xml',
        'views/dokter_menu.xml',
        'views/jadwal_dokter_view.xml',
        'views/perawatan_menu.xml',
        'views/obat_menu.xml',
        'views/penjualan_obat_view.xml',
        'report/report.xml',
        'report/report_perawatan_pdf.xml',
        'wizard/penghasilan_dokter_wizard.xml',
        'report/report_penghasilan_dokter_wizard_pdf.xml',
        'wizard/pengeluaran_pasien_wizard.xml',
        'report/report_pengeluaran_pasien_wizard_pdf.xml',

    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
