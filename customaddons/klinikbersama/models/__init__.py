# -*- coding: utf-8 -*-

from . import models
from . import person
from . import dokter
from . import pasien
from . import jadwal_dokter
from . import perawatan
from . import obat
from . import penjualan_obat