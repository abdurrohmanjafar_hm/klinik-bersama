from email.policy import default
from odoo import api, fields, models


class Dokter(models.Model):
    _name = 'klinikbersama.dokter'
    _description = 'Dokter in klinik bersama that inherit person class'
    _inherit = 'klinikbersama.person'
    _rec_name = 'name'
    
    bidang = fields.Selection(string='Bidang', selection=[('dokter_umum', 'Dokter Umum'), ('dokter_gigi', 'Dokter Gigi'),], required=True)
    jadwal_dokter_ids = fields.One2many(comodel_name='klinikbersama.jadwal.dokter', inverse_name='dokter_id', string='List Jadwal Dokter')
    perawatan_ids = fields.One2many(comodel_name='klinikbersama.perawatan', inverse_name='dokter_id', string='List Perawatan')

    
    
    
    
    
