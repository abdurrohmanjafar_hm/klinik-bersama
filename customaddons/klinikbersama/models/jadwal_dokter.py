from odoo import api, fields, models
from odoo.exceptions import ValidationError


CONST_HARI = ["Senin", 'Selasa', "Rabu", "Kamis", "Jumat"]

class JadwalDokter(models.Model):
    _name = 'klinikbersama.jadwal.dokter'
    _description = 'Jadwal praktek for dokter in klinik bersama'

    dokter_id = fields.Many2one(comodel_name='klinikbersama.dokter', string='Dokter', required=True)
    name = fields.Char(string='Kode Jadwal', readonly=True)
    hari_praktek = fields.Selection(string='Hari Praktek', inverse="_inverse_name", selection=[('0', 'Senin'), ('1', 'Selasa'), ('2', 'Rabu'), ('3', 'Kamis'), ('4', 'Jumat'),], required=True)
    tarif = fields.Integer(string='Tarif')

    # constraint nilai tarif tidak boleh kurang dari nol
    @api.constrains('tarif')
    def _check_tarif(self):
        for rec in self:
            if rec.tarif < 0:
                raise ValidationError("Tarif tidak boleh kurang dari nol")
    
    
    # constraint agar setiap dokter hanya memiliki satu jadwal untuk setiap harinya (tidak boleh ada jadwal pada hari yang sama)
    @api.constrains('dokter_id', 'hari_praktek')
    def _check_jadwal_unique_for_dokter(self):
        for rec in self:
            get_jadwal = self.env['klinikbersama.jadwal.dokter'].search([('dokter_id', '=', rec.dokter_id.id), ('hari_praktek', '=', rec.hari_praktek)])
            print([x.hari_praktek for x in get_jadwal])
            if len(get_jadwal) > 1:
                raise ValidationError("Dokter {} sudah mempunyai jadwal untuk hari {}".format(rec.dokter_id.name, CONST_HARI[int(rec.hari_praktek)]))
    
    
    # fungsi untuk mengenerate kode jadwal berdasarkan hari praktek dan id dari dokter
    def _inverse_name(self):
        for rec in self:
            rec.name = "{}-{}".format(CONST_HARI[int(rec.hari_praktek)], rec.id)