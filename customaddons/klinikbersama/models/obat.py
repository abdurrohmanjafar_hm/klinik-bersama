from odoo import api, fields, models
from odoo.exceptions import ValidationError


class Obat(models.Model):
    _name = 'klinikbersama.obat'
    _description = 'New Description'

    name = fields.Char(string='Nama', required=True)
    stok = fields.Integer(string="Stok")
    harga_jual = fields.Integer(string="Harga Jual")
    
    # constraint untuk stok dan harga jual dari obat tidak boleh kurang dari nol
    @api.constrains('stok', 'harga_jual')
    def _check_tarif(self):
        for rec in self:
            if rec.stok < 0:
                raise ValidationError("Tarif tidak boleh kurang dari nol")
            elif rec.harga_jual < 0:
                raise ValidationError("Harga jual tidak boleh kurang dari nol")