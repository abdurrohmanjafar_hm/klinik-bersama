from odoo import api, fields, models


class Pasien(models.Model):
    _name = 'klinikbersama.pasien'
    _description = 'Pasien in klinik bersama that inherit person class'
    _inherit = 'klinikbersama.person'
    _rec_name = 'name'

    membership = fields.Char(string='Membership', compute="_compute_membership")
    perawatan_ids = fields.One2many(comodel_name='klinikbersama.perawatan', inverse_name='pasien_id', string='Riwayat Perawatan')
    pembelian_ids = fields.One2many(comodel_name='klinikbersama.penjualan.obat', inverse_name='pasien_id', string='Riwayat Pembelian Obat')
    
    # fungsi untuk menentukan status membership dari seorang pasien berdasarkan jumlah pengeluaran pada perawatan dan pembelian obat
    @api.depends('perawatan_ids', 'pembelian_ids')
    def _compute_membership(self):
        for rec in self:
            total_perawatan = [x.biaya_perawatan for x in rec.perawatan_ids if x.state == "done"]
            total_pembelian = [x.total_transaksi for x in rec.pembelian_ids]
            sum_pengeluaran = sum(total_pembelian) + sum(total_perawatan)
            if sum_pengeluaran > 10000000:
                rec.membership = 'VVIP'
            elif sum_pengeluaran > 1000000:
                rec.membership = 'VIP'
            elif sum_pengeluaran > 100000:
                rec.membership = 'Regular'
            else:
                rec.membership = 'Bukan Member'