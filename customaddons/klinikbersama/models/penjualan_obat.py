from odoo import api, fields, models
from odoo.exceptions import ValidationError

class PenjualanObat(models.Model):
    _name = 'klinikbersama.penjualan.obat'
    _description = 'Penjualan obat in klinik bersama'

    name = fields.Char(string='Nomor Transaksi')
    pasien_id = fields.Many2one(comodel_name='klinikbersama.pasien', string='Pembeli', required=True)
    detail_penjualan_obat_ids = fields.One2many(comodel_name='klinikbersama.detail.penjualan.obat', string='list Obat', inverse_name='penjualan_obat_id')
    tanggal_transaksi = fields.Date("Tgl. Transaksi", required=True)
    total_transaksi = fields.Integer(string="Total Transaksi", compute="_compute_total_transaksi")
    
    # fungsi untuk menghitung total transaksi dari setiap pembelian obat dalam satu transaksi
    @api.depends('detail_penjualan_obat_ids')
    def _compute_total_transaksi(self):
        for rec in self:
            total_nilai = sum(self.env['klinikbersama.detail.penjualan.obat'].search([('penjualan_obat_id', '=', rec.id)]).mapped('sub_total'))
            rec.total_transaksi = total_nilai
            
    # fungsi untuk menghandle stok obat terhadap penghapusan objek penjuealan obat               
    def unlink(self):
        for rec in self:
            a = self.env['klinikbersama.detail.penjualan.obat'].search([('penjualan_obat_id', '=', rec.id)])
            for obj in a:
                obj.obat_id.stok += obj.quantity            
        return super(PenjualanObat, self).unlink()
    
    
    # fungsi untuk menghandle stok obat terhadap pengeditan objek penjuealan obat   
    def write(self, values):
        for rec in self:
            a = self.env['klinikbersama.detail.penjualan.obat'].search([('penjualan_obat_id', '=', rec.id)])
            for obj in a:
                obj.obat_id.stok += obj.quantity
                
        record = super(PenjualanObat, self).write(values)
        
        for rec in self:
            b = self.env['klinikbersama.detail.penjualan.obat'].search([('penjualan_obat_id', '=', rec.id)])
            for obj in b:
                if obj in a:
                    obj.obat_id.stok -= obj.quantity
                else:
                    pass
        return record


class DetailPenjualanObat(models.Model):
    _name = 'klinikbersama.detail.penjualan.obat'
    _description = 'Detail penjualan obat in klinik bersama'

    penjualan_obat_id = fields.Many2one(comodel_name='klinikbersama.penjualan.obat', string='Penjualan Obat', ondelete='cascade')
    obat_id = fields.Many2one(comodel_name='klinikbersama.obat', string='Obat', required=True)
    harga_barang = fields.Integer(string="Harga Barang")
    quantity = fields.Integer(string='Quantity')
    sub_total = fields.Integer(string='Harga', compute='_compute_sub_total')
    
    # fungsi untuk menyedikana harga barang berdasarkan obat yang dipilih
    @api.onchange('obat_id')
    def _onchange_obat_id(self):
        self.harga_barang = self.obat_id.harga_jual
    
    # fungsi untuk menghitung sub total dari suatu pembelian obat
    @api.depends('harga_barang', 'quantity')
    def _compute_sub_total(self):
        for rec in self:
            rec.sub_total = rec.quantity * rec.harga_barang
            
    
    # constraint quantity tidak boleh kurang dari 1 atau melebihi stok obat 
    @api.constrains('quantity')
    def _check_quantity(self):
        for rec in self:
            if rec.quantity < 1:
                raise ValidationError("Jumlah pembelian yang anda masukkan untuk {} tidak valid".format(rec.obat_id.name))
            elif rec.quantity > rec.obat_id.stok:
                raise ValidationError("Jumlah pembelian {} melebihi stok yang tersedia, barang {} memiliki stok sebanyak {}.".format(rec.obat_id.name, rec.obat_id.name, rec.obat_id.stok))    
    
    # fungsis untuk menghandle pengurangan stok obat ketika terjadi penjualan
    @api.model
    def create(self, values):
        record = super(DetailPenjualanObat, self).create(values)
        if record.quantity:
            record.obat_id.stok -= record.quantity
        return record
    
    
    
    

    
    
