from odoo import api, fields, models
from odoo.exceptions import ValidationError
from datetime import date

# constant
CONST_HARI = ["Senin", 'Selasa', "Rabu", "Kamis", "Jumat"]
CONST_DISCOUNT = {'Bukan Member': 1, 'Regular': 0.85, 'VIP': 0.75, 'VVIP': 0.6}

class Perawatan(models.Model):
    _name = 'klinikbersama.perawatan'
    _description = 'Perawatan Pasien in klinik bersama'

    name = fields.Char(string='Nomor Perawatan', readonly=True)
    pasien_id = fields.Many2one(comodel_name='klinikbersama.pasien', string='Pasien', inverse='_inverse_name', required=True)
    dokter_id = fields.Many2one(comodel_name='klinikbersama.dokter', string='Dokter', required=True)
    tanggal_perawatan = fields.Date(string='Tgl. Perawatan', required=True)
    biaya_perawatan = fields.Integer(string="Biaya Perawatan", required=True)
    state = fields.Selection(string='Status',
                              selection=[('booking', 'Booking'), ('confirm', 'Confirm'), ('done', 'Done'), ('cancelled', 'Cancelled'),],
                              required=True, readonly=True, default='booking')
    
    
    # button action untuk perubahan state perawatan
    def action_confirm(self):
        self.write({'state': 'confirm'})

    def action_done(self):
        self.write({'state': 'done'})

    def action_cancel(self):
        self.write({'state': 'cancelled'})

    def action_draft(self):
        self.write({'state': 'booking'})
    
    # untuk menghandle penghapusan perawatan, hanya bisa dilakukan apabila booking dan cancelled
    def unlink(self):
        for row in self:
            if row.state != 'booking' and row.state != 'cancelled':
                raise ValidationError("Tdak dapat menghapus jika status BUKAN BOOKING atau CANCELLED")
        return super(Perawatan, self).unlink()    
    
    # untuk menghitung biaya perawatan, terdapat potonhan biaya perawatan yang bergantung pada membership dari pasien
    @api.onchange('tanggal_perawatan', 'pasien_id')
    def _onchange_field_name(self):
        for rec in self:
            if rec.tanggal_perawatan:
                hari_tanggal_dipilih = rec.tanggal_perawatan.weekday()
                jadwal_perawatan = self.env['klinikbersama.jadwal.dokter'].search([('dokter_id', '=', rec.dokter_id.id), ('hari_praktek', '=', str(hari_tanggal_dipilih))])
                rec.biaya_perawatan = jadwal_perawatan.tarif * CONST_DISCOUNT[rec.pasien_id.membership]
    
    
    # pengecekan apakah dokter yang dipilih memeiliki jadwal praktek pada tanggal yang dipilih
    @api.constrains('tanggal_perawatan')
    def _check_tanggal_perawatan(self):
        for rec in self:
            if rec.tanggal_perawatan < date.today():
                raise ValidationError("Tanggal perawatan yang dipilih sudah lewat hari ini, silahkan pilih tanggal lain.")
            hari_tanggal_dipilih = rec.tanggal_perawatan.weekday()
            list_jadwal = self.env['klinikbersama.jadwal.dokter'].search([('dokter_id', '=', rec.dokter_id.id)]).mapped("hari_praktek")
            print(list_jadwal)
            if str(hari_tanggal_dipilih) not in list_jadwal:
                daftar_hari = [CONST_HARI[int(x)] for x in list_jadwal]
                daftar_hari = ", ".join(daftar_hari)
                raise ValidationError("Dokter {} tidak tersedia pada hari {}, Dokter tersedia pada hari {}".format(rec.dokter_id.name, CONST_HARI[int(hari_tanggal_dipilih)], daftar_hari))
    
    
    # fungsi untuk menghasilkan nomor perawatan
    def _inverse_name(self):
        for rec in self:
            pasien_name = rec.pasien_id.name.split()
            first_2_char_list = [x[0:3] for x in pasien_name]
            join_word = "".join(first_2_char_list)
            rec.name = "{}-{}".format(join_word, rec.id)
    