from odoo import api, fields, models
from odoo.exceptions import ValidationError
import re

class Person(models.Model):
    _name = 'klinikbersama.person'
    _description = 'Base info for person in klinik bersama'

    name = fields.Char(string='Nama', required=True)
    no_telp = fields.Char(string='Nomor Telepon', required=True)
    alamat = fields.Char(string='Alamat')
    
    # constraint no telepoon adalah digit angka dengan panjang 8-12 character
    @api.constrains('no_telp')
    def _check_no_telp(self):
        for rec in self:
            if not re.search("^[0-9]{8,12}$", rec.no_telp):
                raise ValidationError("Terjadi kesalahan pada input nomor telepon, pastikan nomor telepon yang anda masukkan valid, digit angka dengan panjang 8-12 character")