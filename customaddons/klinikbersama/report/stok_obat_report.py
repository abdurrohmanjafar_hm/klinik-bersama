from odoo import models, fields

class StokObatReport(models.AbstractModel):
    _name = 'report.klinikbersama.stok_obat_report'
    _inherit = 'report.report_xlsx.abstract'

    tgl_lap = fields.Date.today()

    def generate_xlsx_report(self, workbook, data, obat):
        sheet = workbook.add_worksheet('Daftar Stok Obat')
        bold = workbook.add_format({'bold': True})
        sheet.write(0, 0, str(self.tgl_lap))
        sheet.write(1, 0, 'Nama Obat')
        sheet.write(1, 1, 'Harga Jual')
        sheet.write(1, 2, 'Stok')
        row = 2
        for rec in obat:
            sheet.write(row, 0, rec.name)
            sheet.write(row, 1, rec.harga_jual)
            sheet.write(row, 2, rec.stok)
            row += 1