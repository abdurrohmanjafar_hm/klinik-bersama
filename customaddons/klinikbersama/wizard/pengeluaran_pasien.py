from odoo import fields, models, api
from odoo.exceptions import ValidationError

class PengeluaranPasien(models.TransientModel):
    _name = 'klinikbersama.pengeluaran.pasien'
    _description = 'Report for pengeluaran pasien'

    pasien_id = fields.Many2one(
        comodel_name='klinikbersama.pasien',
        string='Pasien',
        required=False)
    dari_tgl = fields.Date(
        string='Dari Tanggal',
        required=False)
    ke_tgl = fields.Date(
        string='Ke tanggal',
        required=False)

    # constraint dari_tgl tidak boleh lebih dari ke_tgl
    @api.constrains('dari_tgl', 'ke_tgl')
    def _check_field_name(self):
        for rec in self:
            if rec.dari_tgl and rec.ke_tgl:
                if rec.dari_tgl > rec.ke_tgl:
                    raise ValidationError('Tanggal mulai tidak boleh melewati tanggal selesai')
    
    # fungsi untuk menghasilkan report
    def action_pengeluaran_pasien_report(self):
        filter_obat = []
        filter_perawatan = [('state', '=', 'done')]
        pasien_id = self.pasien_id
        membership = self.pasien_id.membership
        dari_tgl = self.dari_tgl
        ke_tgl = self.ke_tgl
        if pasien_id:
            filter_obat += [('pasien_id', '=', pasien_id.id)]
            filter_perawatan += [('pasien_id', '=', pasien_id.id)]
        else:
            raise ValidationError("Pasien tidak boleh kosong")
        if dari_tgl:
            filter_obat += [('tanggal_transaksi', '>=', dari_tgl)]
            filter_perawatan += [('tanggal_perawatan', '>=', dari_tgl)]
        if ke_tgl:
            filter_obat += [('tanggal_transaksi', '<=', ke_tgl)]
            filter_perawatan += [('tanggal_perawatan', '<=', ke_tgl)]

        pengeluaran_perawatan = self.env['klinikbersama.perawatan'].search_read(filter_perawatan)
        pengeluaran_obat = self.env['klinikbersama.penjualan.obat'].search_read(filter_obat)

        data = {
            'form': self.read()[0],
            'membership': membership,
            'pengeluaran_perawatan': pengeluaran_perawatan,
            'pengeluaran_obat': pengeluaran_obat,
        }
        
        return self.env.ref('klinikbersama.report_pengeluaran_pasien_wizard_pdf_action').report_action(self, data=data)