from odoo import api, fields, models
from odoo.exceptions import ValidationError
import calendar


class PenghasilanDokter(models.TransientModel):
    _name = 'klinikbersama.penghasilan.dokter'
    _description = 'report penghasilan dokter untuk suatu durasi tertentu'

    dokter_id = fields.Many2one(comodel_name='klinikbersama.dokter', string='Dokter')
    bulan = fields.Selection(string='Bulan', selection=[('1', 'Januari'), ('2', 'Februari'), ('3', 'Maret'),
                                                        ('4', 'April'), ('5', 'Mei'), ('6', 'Juni'), 
                                                        ('7', 'Juli'), ('8', 'Agustus'), ('9', 'September'), 
                                                        ('10', 'Oktober'), ('11', 'November'), ('12', 'Desember'),])
    
    
    def print_penghasilan_dokter_button(self):
        filter = [('state', '=', 'done')]
        dokter_id = self.dokter_id
        bulan = self.bulan
        
        if dokter_id:
            filter += [('dokter_id', '=', dokter_id.id)]
        else:
            raise ValidationError("Dokter tidak boleh kosong")
        perawatan = self.env['klinikbersama.perawatan'].search_read(filter)
        
        list_perawatan = []
        if bulan:
            for row in perawatan:
                if row['tanggal_perawatan'].month == int(bulan):
                    list_perawatan.append(row)
        else:
            list_perawatan = [row for row in perawatan]
        
        print(list_perawatan)
        sum_biaya_perawatan = 0
        for rec in list_perawatan:
            sum_biaya_perawatan += rec["biaya_perawatan"]
        data_compute = {
                "dokter": dokter_id.name,
                "bulan" : calendar.month_name[int(bulan)],
                "jumlah" : sum_biaya_perawatan,
                "list_perawatan": list_perawatan
                }
        print(data_compute)    
        data = {
            'perawatan': data_compute,
        }
        
        print(data)
        return self.env.ref('klinikbersama.report_penghasilan_dokter_wizard_pdf_action').report_action(self, data=data)